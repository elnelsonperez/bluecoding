<?php

namespace App\Services;

use App\Models\User;

class UserService
{

    public function createUser ($data) {
        return User::create($data);
    }

    public function getUserById($id) {
        return User::find($id);
    }

    public function getUsers ($filters) {
        $query = User::query();

        $fields = [
            'name','email','first_name','last_name'
        ];

        foreach ($fields as $f) {
            if (isset($filters[$f])) {
                $query = $query->where($f,'like',$filters[$f].'%');
            }
        }

        return $query->get();
    }

    public function deleteUser ($id) {
        return User::destroy($id);
    }

    public function userExists ($id) {
        return User::where('id','=',$id)->exists();
    }

    public function getUserGuests ($id) {
        return User::where('host_id',$id)->get();
    }

    public function isUserHost ($id) {
        return User::where('id','=',$id)->where('is_host',true)->exists();
    }

    public function deleteHostGuests ($id) {
        return User::where('host_id',$id)->delete();
    }
    public function getGuestRecommendations ($id, $miles = 50, LocationService $service) {
        $user = User::select(['id','location_lat','location_lng'])->where('id',$id)->get()->first();

        $potentialGuests = User::select(['id','location_lat','location_lng'])
            ->where('id','!=',$id)->get();

        $result = [];

        foreach ($potentialGuests as $guest) {


            $distanceMeters = $service->haversineGreatCircleDistance(
                $user->location_lat,
                $user->location_lng,
                $guest->location_lat,
                $guest->location_lng);


            if ($distanceMeters*0.00062137 <= $miles) {
                $result[] = (int)$guest['id'];
            }

        }


        return $result;
    }

}