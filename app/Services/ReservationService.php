<?php

namespace App\Services;


use App\Models\User;

class ReservationService
{

    public function createReservation ($data) {
        User::whereIn('id',$data['guests'])->update(['host_id' => $data['host']]);
        User::find($data['host'])->update(['is_host' => true]);
        return true;
    }

}