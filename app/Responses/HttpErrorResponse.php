<?php

namespace App\Responses;


use JsonSerializable;

class HttpErrorResponse implements JsonSerializable
{

    private $code;
    /**
     * @var null
     */
    private $message;

    public function __construct($code, $message = null)
    {

        $this->code = $code;
        $this->message = $message;
    }

    public function jsonSerialize()
    {
        return [
            'errorCode' => $this->code,
            'errorMessage' => $this->message
        ];
    }
}