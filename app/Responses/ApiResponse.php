<?php

namespace App\Responses;

use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class ApiResponse extends JsonResponse
{

    public function __construct($data = null, $status = 200, $message, $headers = [], $options = 0)
    {

        $response = [
            'code' => $status,
            'status' => Response::$statusTexts[$status],
            'message' => $message,
            'response' => $data
        ];

        parent::__construct($response, $status, $headers, $options);
    }
}