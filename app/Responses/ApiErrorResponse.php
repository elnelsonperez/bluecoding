<?php

namespace App\Responses;

class ApiErrorResponse extends ApiResponse
{


    public function __construct($errorCode = null, $errorMessage = '',  $headers = [], $options = 0)
    {
        parent::__construct([
            'errorCode' => $errorCode,
            'errorMessage' => $errorMessage
        ], 400, "Error", $headers, $options);
    }
}