<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use App\Responses\ApiErrorResponse;
use App\Responses\ApiResponse;
use App\Services\ReservationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReservationController extends Controller
{


    public function createReservation(Request $request, ReservationService $service)
    {
        try {
            $validator = Validator::make($request->all(), [
                'host' => 'required|numeric|exists:users,id',
                'guests.*' => 'required|numeric|exists:users,id'
            ]);

            if ($validator->fails()) {
                return new ApiErrorResponse(422, "Validation error");
            }

            $validatedData = $validator->valid();
            $service->createReservation($validatedData);

            return new ApiResponse(true, 200, 'Success');
        } catch (\Exception $e) {
            return new ApiErrorResponse(500, "Something went wrong");
        }
    }
}


