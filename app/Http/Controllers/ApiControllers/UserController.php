<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use App\Responses\ApiErrorResponse;
use App\Responses\ApiResponse;
use App\Services\LocationService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function createUser (Request $request, UserService $service) {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|string|email',
                'name' => 'required|string',
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'date_of_birth' => 'required|date',
                'is_host' => 'required|boolean',
                'location_lat' => 'required',
                'location_lng' => 'required'
            ]);

            if ($validator->fails()) {
                return new ApiErrorResponse(422,"Validation error");
            }

            $validatedData = $validator->valid();
            $user = $service->createUser($validatedData);

            return new ApiResponse($user,200,'Success');
        }
        catch (\Exception $e) {
            return new ApiErrorResponse(500,"Something went wrong");
        }

    }

    public function getUser ($id, UserService $service) {
        try {

            $user = $service->getUserById($id);

            if (is_null($user)) {
                return new ApiErrorResponse(404,"Record not found");
            }

            return new ApiResponse($user,200,'Success');
        }
        catch (\Exception $e) {
            return new ApiErrorResponse(500,"Something went wrong");
        }
    }

    public function getUsers (Request $request, UserService $service) {

        try {
            $validator = Validator::make($request->all(), [
                'name' => 'string',
                'email' => 'string',
                'fist_name' => 'string',
                'last_name' => 'string'
            ]);

            if ($validator->fails()) {
                return new ApiErrorResponse(422,"Validation error");
            }

            $validatedData = $validator->valid();
            $users = $service->getUsers($validatedData);

            return new ApiResponse($users,200,'Success');
        }
        catch (\Exception $e) {
            return new ApiErrorResponse(500,"Something went wrong");
        }
    }

    public function deleteUser ($id, UserService $service) {
        try {

            if (!$service->userExists($id)) {
                return new ApiErrorResponse(404,"Record not found");
            }

            $service->deleteUser($id);

            return new ApiResponse(true,200,'Success');
        }
        catch (\Exception $e) {
            return new ApiErrorResponse(500,"Something went wrong");
        }
    }

    public function getUserGuests ($id, UserService $service) {
        try {

            if (!$service->userExists($id)) {
                return new ApiErrorResponse(404,"Record not found");
            }

            if (!$service->isUserHost($id)) {
                return new ApiErrorResponse(404,"The user is not a host");
            }

            $result = $service->getUserGuests($id);

            return new ApiResponse($result,200,'Success');
        }
        catch (\Exception $e) {
            return new ApiErrorResponse(500,"Something went wrong");
        }
    }

    public function deleteUserGuests($id, UserService $service)
    {
        try {

            if (!$service->userExists($id)) {
                return new ApiErrorResponse(404,"Record not found");
            }

            if (!$service->isUserHost($id)) {
                return new ApiErrorResponse(404,"The user is not a host");
            }

            $service->deleteHostGuests($id);

            return new ApiResponse(true,200,'Success');
        }
        catch (\Exception $e) {
            return new ApiErrorResponse(500,"Something went wrong");
        }
    }

    public function getGuestRecommendations ($id, UserService $service, LocationService $locService) {
        try {

            if (!$service->userExists($id)) {
                return new ApiErrorResponse(404,"Record not found");
            }

            $guest_ids = $service->getGuestRecommendations($id,50,$locService);

            return new ApiResponse(['uids' => array_merge([(int)$id], $guest_ids)] , 200,'Success');
        }
        catch (\Exception $e) {
            return new ApiErrorResponse(500,"Something went wrong");
        }
    }

}