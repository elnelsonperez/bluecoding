<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public $hidden = ['host_id'];

    public function guests () {
        return $this->hasMany(User::class,'host_id');
    }

    public function host () {
        return $this->belongsTo(User::class,'host_id');
    }

}
