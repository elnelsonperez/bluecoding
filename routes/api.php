<?php

Route::group(['prefix' => 'v1'], function () {
    Route::post('users/create', 'ApiControllers\UserController@createUser')->name('createUser');
    Route::get('users', 'ApiControllers\UserController@getUsers')->name('getUsers');
    Route::get('users/{id}', 'ApiControllers\UserController@getUser')->name('getUser');
    Route::get('users/{id}/guests', 'ApiControllers\UserController@getUserGuests')->name('getUsersGuests');
    Route::delete('users/{id}', 'ApiControllers\UserController@deleteUser')->name('deleteUser');

    Route::post('reservations/create', 'ApiControllers\ReservationController@createReservation')->name('createReservation');
    Route::delete('reservations/host/{id}/guests', 'ApiControllers\UserController@deleteUserGuests')->name('deleteUserGuests');

    Route::get('users/recommendations/{id}', 'ApiControllers\UserController@getGuestRecommendations')->name('getGuestRecommendations');

});


