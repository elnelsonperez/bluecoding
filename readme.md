## Bluecoding Reservation Coding Test
A simple reservations API for a coding evaluation, built with Laravel.

##### [Documentation](https://bluecoding-reservations.restlet.io)

### Instructions
You need to have [Vagrant](https://www.vagrantup.com/) installed.

1.  Clone the repository
    ```
    git clone https://elnelsonperez@bitbucket.org/elnelsonperez/bluecoding.git
    ```
2.  Rename or copy `.env.example` to `.env`.
3.  Install dependencies `php composer install`
4.  Run `php vendor/bin/homestead make` to setup homestead. You can modify the `Homestead.yaml` file if you need to tweak the VM settings.
5.  Add `homestead.test` to your `hosts` file and point it to the VMs IP, `192.168.10.10` by default.
6.  Run `vagrant up`
7.  Run `vagrant ssh` to access the VM and go to the `code` directory.
7.  Run Laravel migrations with `php artisan migrate` from inside the VM.

The API should be accesible at `http://homestead.test/api/v1` at this point.

Tests are ran by running `phpunit` from inside the VM, or using your own phpunit executable in the host machine.