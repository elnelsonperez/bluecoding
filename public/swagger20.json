{
  "swagger" : "2.0",
  "info" : {
    "description" : "A reservations API as a technical evaluation of PHP and Laravel.",
    "version" : "1.0.0",
    "title" : "Bluecoding",
    "contact" : {
      "name" : "Nelson Perez",
      "email" : "me@nelsonperez.net"
    }
  },
  "host" : "homestead.test",
  "basePath" : "/api/v1",
  "schemes" : [ "http" ],
  "consumes" : [ "application/json" ],
  "produces" : [ "application/json" ],
  "paths" : {
    "/users/create" : {
      "post" : {
        "tags" : [ "Users" ],
        "summary" : "createUser",
        "description" : "Creates a new user.",
        "operationId" : "createUser",
        "consumes" : [ ],
        "parameters" : [ {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/User"
          },
          "x-examples" : {
            "application/json" : "{  \n   \"name\":\"John Smith\",\n   \"email\":\"john@gmail.com\",\n   \"first_name\":\"John\",\n   \"last_name\":\"Smith\",\n   \"date_of_birth\":\"1978-05-25\",\n   \"is_host\":0,\n   \"location_lat\":\"19.4588903229264\",\n   \"location_lng\":\"-70.6902262184282\"\n}"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Status 200",
            "schema" : {
              "type" : "object",
              "description" : "Returns the created user."
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 200,\n    \"status\": \"OK\",\n    \"message\": \"Success\",\n    \"response\": {\n        \"name\": \"John Smith\",\n        \"email\": \"john@gmail.com\",\n        \"first_name\": \"John\",\n        \"last_name\": \"Smith\",\n        \"date_of_birth\": \"1978-05-25\",\n        \"is_host\": 0,\n        \"location_lat\": \"19.4588903229264\",\n        \"location_lng\": \"-70.6902262184282\",\n        \"id\": 1\n    }\n}"
            }
          },
          "400" : {
            "description" : "Status 400",
            "schema" : {
              "type" : "object",
              "description" : "Response returned when the user could not be created."
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 400,\n    \"status\": \"Bad Request\",\n    \"message\": \"Error\",\n    \"response\": {\n        \"errorCode\": 422,\n        \"errorMessage\": \"Validation error\"\n    }\n}\n"
            }
          }
        }
      }
    },
    "/users" : {
      "get" : {
        "tags" : [ "Users" ],
        "summary" : "getUsers",
        "description" : "Fetches a list of users.",
        "operationId" : "getUsers",
        "parameters" : [ {
          "name" : "name",
          "in" : "query",
          "required" : false,
          "type" : "string",
          "description" : "Filter by name, starting with",
          "x-example" : "John"
        }, {
          "name" : "email",
          "in" : "query",
          "required" : false,
          "type" : "string",
          "description" : "Filter by email, starting with",
          "x-example" : "john@"
        }, {
          "name" : "first_name",
          "in" : "query",
          "required" : false,
          "type" : "string",
          "description" : "Filter by first_name, starting with"
        }, {
          "name" : "last_name",
          "in" : "query",
          "required" : false,
          "type" : "string",
          "description" : "Filter by last_name, starting with"
        } ],
        "responses" : {
          "200" : {
            "description" : "Status 200",
            "schema" : {
              "type" : "object",
              "description" : "Returns a JSON array of users matching the request parameters, or all users, if no parameters where provided."
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 200,\n    \"status\": \"OK\",\n    \"message\": \"Success\",\n    \"response\": [\n        {\n            \"id\": 1,\n            \"email\": \"john@gmail.com\",\n            \"name\": \"John Smith\",\n            \"first_name\": \"John\",\n            \"last_name\": \"Smith\",\n            \"date_of_birth\": \"1978-05-25\",\n            \"is_host\": 0,\n            \"location_lat\": \"19.45889032\",\n            \"location_lng\": \"-70.69022622\"\n        }\n    ]\n}\n"
            }
          },
          "400" : {
            "description" : "Status 400",
            "schema" : {
              "type" : "object",
              "description" : "An unexpected error ocurred."
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 400,\n    \"status\": \"Bad Request\",\n    \"message\": \"Error\",\n    \"response\": {\n        \"errorCode\": 500,\n        \"errorMessage\": \"Something went wrong\"\n    }\n}\n"
            }
          }
        }
      }
    },
    "/users/{id}/guests" : {
      "get" : {
        "tags" : [ "Users" ],
        "summary" : "getUserGuests",
        "description" : "Get a user's guests",
        "operationId" : "getUserGuests",
        "parameters" : [ ],
        "responses" : {
          "200" : {
            "description" : "Status 200",
            "schema" : {
              "type" : "object",
              "description" : "Returns the user's guests."
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 200,\n    \"status\": \"OK\",\n    \"message\": \"Success\",\n    \"response\": [\n        {\n            \"id\": 3,\n            \"email\": \"john2@gmail.com\",\n            \"name\": \"Jose The Man\",\n            \"first_name\": \"John\",\n            \"last_name\": \"Perez\",\n            \"date_of_birth\": \"1985-05-25\",\n            \"is_host\": 0,\n            \"location_lat\": \"19.45889032\",\n            \"location_lng\": \"-70.69022622\"\n        }\n    ]\n}"
            }
          },
          "400" : {
            "description" : "Status 400",
            "schema" : {
              "type" : "object",
              "description" : "User not found, the user is not a host, or unexpected error ocurred."
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 400,\n    \"status\": \"Bad Request\",\n    \"message\": \"Error\",\n    \"response\": {\n        \"errorCode\": 404,\n        \"errorMessage\": \"Record not found\"\n    }\n}"
            }
          }
        }
      },
      "parameters" : [ {
        "name" : "id",
        "in" : "path",
        "required" : true,
        "type" : "string"
      } ]
    },
    "/users/{id}" : {
      "get" : {
        "tags" : [ "Users" ],
        "summary" : "getUser",
        "description" : "Fetches a specific user.",
        "operationId" : "getUser",
        "parameters" : [ ],
        "responses" : {
          "200" : {
            "description" : "Successfull response",
            "schema" : {
              "type" : "object",
              "description" : "A JSON object of an user."
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 200,\n    \"status\": \"OK\",\n    \"message\": \"Success\",\n    \"response\": {\n        \"id\": 1,\n        \"email\": \"john@gmail.com\",\n        \"name\": \"John Smith\",\n        \"first_name\": \"John\",\n        \"last_name\": \"Smith\",\n        \"date_of_birth\": \"1978-05-25\",\n        \"is_host\": 0,\n        \"location_lat\": \"19.45889032\",\n        \"location_lng\": \"-70.69022622\"\n    }\n}\n"
            }
          },
          "400" : {
            "description" : "Status 400",
            "schema" : {
              "type" : "object",
              "description" : "Response returned when the user could not be fetched."
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 400,\n    \"status\": \"Bad Request\",\n    \"message\": \"Error\",\n    \"response\": {\n        \"errorCode\": 404,\n        \"errorMessage\": \"Record not found\"\n    }\n}\n"
            }
          }
        }
      },
      "delete" : {
        "tags" : [ "Users" ],
        "summary" : "deleteUser",
        "description" : "Deletes a specific user",
        "operationId" : "deleteUser",
        "parameters" : [ ],
        "responses" : {
          "200" : {
            "description" : "Successful response.",
            "schema" : {
              "type" : "object",
              "description" : "The user was successfully deleted."
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 200,\n    \"status\": \"OK\",\n    \"message\": \"Success\",\n    \"response\": true\n}"
            }
          },
          "400" : {
            "description" : "Status 400",
            "schema" : {
              "type" : "object",
              "description" : "The user specified to be deleted does not exist, or general error occured."
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 400,\n    \"status\": \"Bad Request\",\n    \"message\": \"Error\",\n    \"response\": {\n        \"errorCode\": 404,\n        \"errorMessage\": \"Record not found\"\n    }\n}\n"
            }
          }
        }
      },
      "parameters" : [ {
        "name" : "id",
        "in" : "path",
        "required" : true,
        "type" : "string"
      } ]
    },
    "/reservations/create" : {
      "post" : {
        "tags" : [ "Reservations" ],
        "summary" : "createReservation",
        "description" : "Creates a new reservation.",
        "consumes" : [ ],
        "parameters" : [ {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "type" : "object",
            "description" : "Expects the host user id and the guests ids as an array."
          },
          "x-examples" : {
            "application/json" : "{\n  \"host\": 1,\n  \"guests\": [2, 3]\n}"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Status 200",
            "schema" : {
              "type" : "object"
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 200,\n    \"status\": \"OK\",\n    \"message\": \"Success\",\n    \"response\": true\n}"
            }
          },
          "400" : {
            "description" : "Status 400",
            "schema" : {
              "type" : "object",
              "description" : "The host or guests do not exist, or general error ocurred."
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 400,\n    \"status\": \"Bad Request\",\n    \"message\": \"Error\",\n    \"response\": {\n        \"errorCode\": 422,\n        \"errorMessage\": \"Validation error\"\n    }\n}"
            }
          }
        }
      }
    },
    "/reservations/host/{id}/guests" : {
      "delete" : {
        "tags" : [ "Reservations" ],
        "summary" : "deleteUserGuests",
        "description" : "Deletes guests from a reservation.",
        "operationId" : "deleteUserGuests",
        "parameters" : [ ],
        "responses" : {
          "200" : {
            "description" : "Status 200",
            "schema" : {
              "type" : "object"
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 200,\n    \"status\": \"OK\",\n    \"message\": \"Success\",\n    \"response\": true\n}"
            }
          },
          "400" : {
            "description" : "Status 400",
            "schema" : {
              "type" : "object",
              "description" : "The user does not exist or an unexpected error occurred."
            },
            "examples" : {
              "application/json" : "{\n    \"code\": 400,\n    \"status\": \"Bad Request\",\n    \"message\": \"Error\",\n    \"response\": {\n        \"errorCode\": 404,\n        \"errorMessage\": \"Record not found\"\n    }\n}"
            }
          }
        }
      },
      "parameters" : [ {
        "name" : "id",
        "in" : "path",
        "required" : true,
        "type" : "string"
      } ]
    }
  },
  "definitions" : {
    "User" : {
      "type" : "object",
      "required" : [ "date_of_birth", "email", "first_name", "is_host", "last_name", "location_lat", "location_lng", "name" ],
      "properties" : {
        "name" : {
          "type" : "string"
        },
        "email" : {
          "type" : "string"
        },
        "first_name" : {
          "type" : "string"
        },
        "last_name" : {
          "type" : "string"
        },
        "is_host" : {
          "type" : "boolean"
        },
        "date_of_birth" : {
          "type" : "string",
          "format" : "date"
        },
        "location_lat" : {
          "type" : "string"
        },
        "location_lng" : {
          "type" : "string"
        }
      },
      "description" : "An user object.",
      "example" : "{  \n   \"name\":\"John Smith\",\n   \"email\":\"john@gmail.com\",\n   \"first_name\":\"John\",\n   \"last_name\":\"Smith\",\n   \"date_of_birth\":\"1978-05-25\",\n   \"is_host\":0,\n   \"location_lat\":\"19.4588903229264\",\n   \"location_lng\":\"-70.6902262184282\"\n}"
    }
  }
}