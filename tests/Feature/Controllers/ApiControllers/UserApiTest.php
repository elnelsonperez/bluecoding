<?php

namespace Tests\Feature\Controllers\ApiControllers;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserApiTest extends TestCase
{

    use RefreshDatabase;

    private static $user;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        self::$user = [
            'name' => "John Smith",
            'email' => 'john@gmail.com',
            'first_name' => "John",
            'last_name' => 'Smith',
            'date_of_birth' => '1978-05-25',
            'is_host' => 0,
            'location_lat' => "19.4588903229264",
            'location_lng' => "-70.6902262184282"
        ];

    }

    public function testCreateUser()
    {
        $response = $this->json( 'POST', route('createUser'),['name' => 'bad'])->assertJson([
            'code' => 400
        ]);

        $response->assertStatus(400);

        $response = $this->json( 'POST', route('createUser'),self::$user)->assertJson([
            'code' => 200,
            'status' => 'OK',
            'response' => self::$user
        ]);

        $response->assertStatus(200);
    }


    public function testGetSingleUser () {

        $user = User::create(self::$user);
        $response = $this->json( 'GET', route('getUser',['id' => $user->id]))->assertJson([
            'code' => 200,
            'status' => 'OK',
            'response' => self::$user
        ]);
        $response->assertStatus(200);
    }


    public function testGetUsers () {
        $user1 = User::create(self::$user);
        $user2 = User::create(array_merge(self::$user, ['email' => 'test@gmail.com']));
        $response = $this->json( 'GET', route('getUsers'))->assertJson([
            'code' => 200,
            'status' => 'OK',
            'response' => [
                $user1->toArray(),
                $user2->toArray()
            ]
        ]);

        $response->assertStatus(200);
        $response = $this->json( 'GET', route('getUsers').'?email=test')->assertJson([
            'code' => 200,
            'status' => 'OK',
            'response' => [
                $user2->toArray()
            ]
        ]);

        $response->assertStatus(200);

        $response = $this->json( 'GET', route('getUsers').'?email=aaaaa')->assertJson([
            'code' => 200,
            'status' => 'OK',
            'response' => []
        ]);

        $response->assertStatus(200);
    }

    public function testDeleteUser () {
        $user1 = User::create(self::$user);
        $this->assertNotNull(User::find($user1->id));
        $response = $this->json( 'DELETE', route('deleteUser',['id' => $user1->id]))->assertJson([
            'code' => 200,
            'status' => 'OK'
        ]);

        $this->assertNull(User::find($user1->id));
        $response->assertStatus(200);
    }

    public function testGetUserGuests () {
        $user1 = User::create(array_merge(self::$user,['is_host' => 1]));
        $user2 = User::create(array_merge(self::$user, ['email' => 'test@gmail.com','host_id' => $user1->id]));
        $user3 = User::create(array_merge(self::$user, ['email' => 'test2@gmail.com','host_id' => $user1->id]));
        $response = $this->json( 'GET', route('getUsersGuests',['id'=>$user1->id]))->assertJson([
            'code' => 200,
            'response' => [
                $user2->toArray(),
                $user3->toArray()
            ]
        ]);
        $response->assertStatus(200);

    }

    public function testDeleteUserGuests () {
        $user1 = User::create(array_merge(self::$user,['is_host' => 1]));
        $user2 = User::create(array_merge(self::$user, ['email' => 'test@gmail.com','host_id' => $user1->id]));
        $user3 = User::create(array_merge(self::$user, ['email' => 'test2@gmail.com','host_id' => $user1->id]));

        $response = $this->json( 'DELETE', route('deleteUserGuests',['id'=>$user1->id]))->assertJson([
            'code' => 200,
            'response' => true
        ]);

        $this->assertNull(User::find($user2->id));
        $this->assertNull(User::find($user3->id));
        $response->assertStatus(200);
    }

    public function testGuestRecommendations () {
        $user1 = User::create(self::$user);
        $user2 = User::create(array_merge(self::$user, //Within
                [
                    'email' => 'test@gmail.com',
                    'location_lat' => '19.4623701940731',
                    'location_lng' => '-70.6844755623003'
                ]
            )
        );

        $user3 = User::create(array_merge(self::$user, //Within
                [
                    'email' => 'test2@gmail.com',
                    'location_lat' => '19.4844755982692',
                    'location_lng' => '-71.7336570050537'
                ]
            )
        );

        $response = $this->json( 'GET', route('getGuestRecommendations',
                ['id' => $user1->id]))->assertJson([
            'code' => 200,
            'status' => 'OK',
            'response' => [
              'uids' => [
                  $user1->id,
                  $user2->id,
                  ]
            ]
        ]);

        $response->assertStatus(200);
    }
}
