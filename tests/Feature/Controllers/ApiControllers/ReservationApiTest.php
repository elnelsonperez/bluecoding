<?php

namespace Tests\Feature\Controllers\ApiControllers;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReservationApiTest extends TestCase
{

   use RefreshDatabase;

    private static $user;

    public static function setUpBeforeClass()
   {
       parent::setUpBeforeClass();
       self::$user = [ 'name' => "John Lokillo",
           'email' => 'john@gmail.com',
           'first_name' => "John",
           'last_name' => 'Smith',
           'date_of_birth' => '1978-05-25',
           'is_host' => 0,
           'location_lat' => "19.4588903229264",
           'location_lng' => "-70.6902262184282"
       ];
   }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateReservation()
    {
        $response = $this->json( 'POST', route('createReservation'),['name' => 'bad'])->assertJson([
            'code' => 400
        ]);

        $response->assertStatus(400);

        $user1 = User::create(self::$user);
        $user2 = User::create(array_merge(self::$user, ['email' => 'test@gmail.com']));
        $user3 = User::create(array_merge(self::$user, ['email' => 'test2@gmail.com']));
        $response = $this->json( 'POST', route('createReservation'),[
            'host' => $user1->id,
            'guests' => [$user2->id, $user3->id]
        ])->assertJson([
            'code' => 200,
            'status' => 'OK',
            'response' => true
        ]);
        $this->assertTrue(User::find($user2->id)->host_id == $user1->id);

        $response->assertStatus(200);
    }


}
